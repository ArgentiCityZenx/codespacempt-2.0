# Introducing CodespaceMPT 2

Yes! We are not gone! We have been reworking things for you all

## HTML Site Tester with Favicon Support

Originally our app only tested sites. Now we have made favicon and title support!

### Responsive Markdown Tester

We even have a markdown tester! It is more responsive than the preview for Gitlab! What?!

#### Git terminal

Run the commands that are here in the terminal to see people's repositories on Git.

```
github [username, repo]
gitlab [username, repo]
githubfile [username, repo, destination]
gitlabfile [username, repo, destination]
```

